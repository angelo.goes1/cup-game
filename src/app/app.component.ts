import { Component, ElementRef, ViewChild } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'cup-game';
  n = 3;
  chosen: number = Math.trunc(Math.random() * 3);
  cupPositions: string[] = ['A', 'B', 'C'];

  constructor() {
    console.log(this.chosen);
    
  }

  cup(num: number) {
    console.log(num);
    if (num == this.chosen) {
      alert('acertou!');
    } else {
      alert('errou!');
    }
  }

  spin() {
    console.log('spin start');
    
    var counter = 0;
    var max_count = 10;
    setInterval(() => {
      if (counter < max_count) {
        this.toggle();
      }
      counter++;
      console.log(counter);
      
    }, 400);
  }

  toggle() {
    console.log('toggle these two:');

    var first = Math.trunc(Math.random() * 3);
    var second = Math.trunc(Math.random() * 3);

    while (first === second) second = Math.trunc(Math.random() * 3);

    var positions = [first, second];
    positions.sort();

    console.log(positions);

    if (positions[0] == 0 && positions[1] == 1) {
      var cup_0 = this.cupPositions[0];
      var cup_1 = this.cupPositions[1];

      document.getElementById(cup_0)!.style.transform += 'translateX(200%)';
      document.getElementById(cup_1)!.style.transform += 'translateX(-200%)';

      this.cupPositions[0] = cup_1;
      this.cupPositions[1] = cup_0;
    } else if (positions[0] == 1 && positions[1] == 2) {
      var cup_1 = this.cupPositions[1];
      var cup_2 = this.cupPositions[2];

      document.getElementById(cup_1)!.style.transform += 'translateX(200%)';
      document.getElementById(cup_2)!.style.transform += 'translateX(-200%)';

      this.cupPositions[1] = cup_2;
      this.cupPositions[2] = cup_1;
    } else if (positions[0] == 0 && positions[1] == 2) {
      var cup_0 = this.cupPositions[0];
      var cup_2 = this.cupPositions[2];

      document.getElementById(cup_0)!.style.transform += 'translateX(400%)';
      document.getElementById(cup_2)!.style.transform += 'translateX(-400%)';

      this.cupPositions[0] = cup_2;
      this.cupPositions[2] = cup_0;
    }
  }
}
